package pl.michalkruczek.myirc;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import layout.IRCFramgemnt;
import layout.LANFragment;

public class LoginActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }


    public void IRConClick(View view) {
        Toast.makeText(getApplicationContext(), "IRC", Toast.LENGTH_SHORT).show();
        IRCFramgemnt irc = new IRCFramgemnt();
        loadFragment(irc);
    }

    public void LANonClick(View view) {
        Toast.makeText(getApplicationContext(), "LAN", Toast.LENGTH_SHORT).show();
        LANFragment lan = new LANFragment();
        loadFragment(lan);
    }

    public void loadFragment(Fragment fragment){

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.containerForFragments, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() > 0){
            getSupportFragmentManager().popBackStack();
        }else{
            super.onBackPressed();
        }
    }
}
