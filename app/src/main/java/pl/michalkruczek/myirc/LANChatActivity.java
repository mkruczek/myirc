package pl.michalkruczek.myirc;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.greendao.database.Database;

import java.util.Date;
import java.util.List;

import pl.michalkruczek.myirc.db.Archive;
import pl.michalkruczek.myirc.db.ArchiveDao;
import pl.michalkruczek.myirc.db.DaoMaster;
import pl.michalkruczek.myirc.db.DaoSession;

public class LANChatActivity extends AppCompatActivity {

    private static final String TAG = "myLog";
    private Context context;

    private EditText chat_msg_EditText;

    private DaoSession daoSession;
    private ArchiveDao archiveDao;
    private List<Archive> archives;

    private RecyclerView recyclerView;
    private LinearLayoutManager llm;
    private LANChatAdapter lanChatAdapter;

    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lanchat);

        context = LANChatActivity.this;

        DaoMaster.DevOpenHelper helperDB = new DaoMaster.DevOpenHelper(context, "users.db");
        Database db = helperDB.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
        archiveDao = daoSession.getArchiveDao();
        archives = archiveDao.queryBuilder().list();

        recyclerView = (RecyclerView) findViewById(R.id.chat_recyclerView);
        llm = new LinearLayoutManager(context);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), llm.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(llm);
        lanChatAdapter = new LANChatAdapter(archives, context);
        recyclerView.setAdapter(lanChatAdapter);


        chat_msg_EditText = (EditText) findViewById(R.id.chat_msg_EditText);

        SharedPreferences sp = getSharedPreferences("myFile", Context.MODE_PRIVATE);
        name = sp.getString(getString(R.string.nick_lan), "def");



    }


    public void chatSendButton(View view) {
        if(chat_msg_EditText.getText().length() != 0){

            Archive archive = new Archive();
            archive.setUser(name);
            archive.setDate(new Date());
            archive.setMessage(chat_msg_EditText.getText().toString());

            archiveDao.insert(archive);
            archives = archiveDao.queryBuilder().list();
            recyclerView.setAdapter(new LANChatAdapter(archives, view.getContext()));

            chat_msg_EditText.setText("");

        } else {
            Toast.makeText(getApplicationContext(), "empty msg", Toast.LENGTH_LONG).show();
        }
    }



}
