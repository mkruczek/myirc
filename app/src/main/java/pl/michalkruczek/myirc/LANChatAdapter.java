package pl.michalkruczek.myirc;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.greenrobot.greendao.database.Database;

import java.text.SimpleDateFormat;
import java.util.List;

import pl.michalkruczek.myirc.db.Archive;
import pl.michalkruczek.myirc.db.ArchiveDao;
import pl.michalkruczek.myirc.db.DaoMaster;
import pl.michalkruczek.myirc.db.DaoSession;

/**
 * Created by RENT on 2017-07-21.
 */

public class LANChatAdapter extends RecyclerView.Adapter<LANChatAdapter.MyViewHolder> {


    private List<Archive> archiveList;
    private Context context;

    public LANChatAdapter(List<Archive> archiveList, Context context) {
        this.archiveList = archiveList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_for_recycler_view, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final Archive CurrentArchive = archiveList.get(position);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
        String dateString = dateFormat.format(CurrentArchive.getDate());

        holder.RV_nick_data.setText("[" + CurrentArchive.getUser() + "]" +" at " + dateString);
        holder.RV_main_msg.setText(CurrentArchive.getMessage());
        holder.RV_deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "users.db");
                Database db = helper.getWritableDb();
                DaoSession daoSession = new DaoMaster(db).newSession();
                ArchiveDao archiveDao = daoSession.getArchiveDao();
                archiveDao.delete(CurrentArchive);

                archiveList = archiveDao.queryBuilder().list();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return archiveList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView RV_nick_data;
        TextView RV_main_msg;
        ImageButton RV_deleteButton;

        public MyViewHolder(View itemView) {
            super(itemView);
            RV_nick_data = (TextView) itemView.findViewById(R.id.RV_nick_data);
            RV_main_msg = (TextView) itemView.findViewById(R.id.RV_main_msg);
            RV_deleteButton = (ImageButton) itemView.findViewById(R.id.RV_deleteButton);
        }
    }
}
