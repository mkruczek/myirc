package com.michalkruczek;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

import java.io.IOException;

public class MyGenerator {

    public static void main(String[] args) {

        Schema schema = new Schema(1, "pl.michalkruczek.myirc.db");

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema, "./app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables(Schema schema) {

        Entity msgArchive = schema.addEntity("Archive");
        msgArchive.addIdProperty().autoincrement().primaryKey();
        msgArchive.addStringProperty("user");
        msgArchive.addDateProperty("date");
        msgArchive.addStringProperty("message");
    }
}
